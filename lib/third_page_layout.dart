import 'package:flutter/material.dart';
import 'package:food/fourth_page_layout.dart';
import 'package:food/second_page_layout.dart';

class ThirdPageLayout extends StatelessWidget {
  const ThirdPageLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(children: [
            Row(
              children: [
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                      return const SecondPageLayout();
                    },));
                  },
                  child: Container(
                    margin: const EdgeInsets.only(top: 30, left: 20),
                    child: const Icon(Icons.arrow_back_rounded, size: 40),
                  ),
                ),
                Container(
                  height: 50,
                  width: 50,
                  margin: const EdgeInsets.only(top: 30, left: 270),
                  decoration: const BoxDecoration(
                      color: Color.fromRGBO(255, 107, 1, 1),
                      borderRadius: BorderRadius.all(Radius.circular(50))),
                  child: const Icon(
                    Icons.thumb_up_off_alt_outlined,
                    size: 40,
                    color: Colors.white,
                  ),
                )
              ],
            ),
            Container(
              margin: const EdgeInsets.only(top: 30),
              alignment: Alignment.center,
              child: const Text(
                "Mushrooms",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.only(top: 30),
              child: Image.asset(
                "assets/images/Screenshot 2023-02-01 191857.png",
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 20, left: 120),
              child: Row(children: [
                Container(
                  alignment: Alignment.center,
                  width: 40,
                  decoration: const BoxDecoration(
                      color: Color.fromRGBO(255, 107, 1, 1),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          bottomLeft: Radius.circular(40))),
                  child: const Text(
                    "-",
                    style: TextStyle(fontSize: 40, color: Colors.white),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 10),
                  width: 80,
                  height: 50,
                  child: const Text(
                    "2",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 30),
                  ),
                ),
                Container(
                  height: 45,
                  alignment: Alignment.center,
                  width: 40,
                  decoration: const BoxDecoration(
                      color: Color.fromRGBO(255, 107, 1, 1),
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(40),
                          bottomRight: Radius.circular(40))),
                  child: const Text(
                    "+",
                    style: TextStyle(fontSize: 30, color: Colors.white),
                  ),
                ),
              ]),
            ),
            Row(
              children: [
                Container(
                    margin: const EdgeInsets.only(top: 20, left: 30),
                    child: Column(
                      children: const [
                        Text(
                          "Size\n",
                          style: TextStyle(fontSize: 15),
                        ),
                        Text(
                          "Medium",
                          style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                        )
                      ],
                    )),
                Container(
                    margin: const EdgeInsets.only(top: 20, left: 60),
                    child: Column(
                      children: const [
                        Text(
                          "Weight\n",
                          style: TextStyle(fontSize: 15),
                        ),
                        Text(
                          "400gr",
                          style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                        )
                      ],
                    )),
                Container(
                    margin: const EdgeInsets.only(top: 20, left: 60),
                    child: Column(
                      children: const [
                        Text(
                          "Price\n",
                          style: TextStyle(fontSize: 15),
                        ),
                        Text(
                          "21.00",
                          style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                        )
                      ],
                    ))
              ],
            ),
            Container(
                margin: const EdgeInsets.only(top: 20, left: 30),
                child: Row(
                  children: const [
                    Text(
                      "Detail\t\t",
                      style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      "Review",
                      style: TextStyle(fontSize: 25),
                    )
                  ],
                )),
            Container(
              margin: const EdgeInsets.only(top: 10,left: 5),
              width: 340,
              child: const Text(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut arcu dolor, vulputate ut tellus cursus, molestie porta justo. Sed eleifend"),),
            InkWell(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                  return FourthPageLayout();
                },));
              },
              child: Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.only(top: 50, left: 10),
                height: 60,
                width: 350,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(40)),
                  color: Color.fromRGBO(255, 107, 1, 1),
                ),
                child: const Text(
                  "Add to cart",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
