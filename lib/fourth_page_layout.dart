import 'package:flutter/material.dart';
import 'package:food/third_page_layout.dart';

class FourthPageLayout extends StatelessWidget {
  const FourthPageLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(children: [
            Row(
              children: [
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) {
                        return const ThirdPageLayout();
                      },
                    ));
                  },
                  child: Container(
                    margin: const EdgeInsets.only(top: 30, left: 20),
                    child: const Icon(Icons.arrow_back_rounded, size: 40),
                  ),
                ),
                Container(
                  height: 50,
                  width: 50,
                  margin: const EdgeInsets.only(top: 30, left: 270),
                  decoration: const BoxDecoration(
                      color: Color.fromRGBO(255, 107, 1, 1),
                      borderRadius: BorderRadius.all(Radius.circular(50))),
                  child: const Icon(
                    Icons.delete,
                    size: 40,
                    color: Colors.white,
                  ),
                )
              ],
            ),
            Container(
              margin: const EdgeInsets.only(top: 30),
              alignment: Alignment.center,
              child: const Text(
                "My Cart",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
              ),
            ),
            Column(
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 10, top: 20),
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(30))),
                  height: 100,
                  width: 400,
                  child: Row(
                    children: [
                      SizedBox(
                        height: 100,
                        child: Image.asset(
                            "assets/images/Screenshot 2023-02-01 183125.png"),
                      ),
                      Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                                top: 20, bottom: 10, right: 15),
                            child: const Text(
                              "Hamburger",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Row(children: [
                            Container(
                              margin: const EdgeInsets.only(left: 20),
                              alignment: Alignment.center,
                              width: 20,
                              decoration: const BoxDecoration(
                                  color: Color.fromRGBO(255, 107, 1, 1),
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(40),
                                      bottomLeft: Radius.circular(40))),
                              child: const Text(
                                "-",
                                style: TextStyle(fontSize: 20, color: Colors.white),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topCenter,
                              // padding: const EdgeInsets.only(top: 10),
                              width: 40,
                              height: 25,
                              child: const Text(
                                "2",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 20),
                              ),
                            ),
                            Container(
                              height: 23,
                              alignment: Alignment.center,
                              width: 20,
                              decoration: const BoxDecoration(
                                  color: Color.fromRGBO(255, 107, 1, 1),
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(40),
                                      bottomRight: Radius.circular(40))),
                              child: const Text(
                                "+",
                                style: TextStyle(fontSize: 20, color: Colors.white),
                              ),
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              margin: const EdgeInsets.only(left: 10),
                              child: const Text(
                                "18.00",
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Color.fromRGBO(255, 107, 1, 1)),
                              ),
                            )
                          ]),
                        ],
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 65, top: 70),
                        decoration: const BoxDecoration(
                          borderRadius:
                              BorderRadius.only(topLeft: Radius.circular(10)),
                          color: Color.fromRGBO(255, 107, 1, 1),
                        ),
                        child: const Icon(
                          Icons.add,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 10, top: 20),
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(30))),
                  height: 100,
                  width: 400,
                  child: Row(
                    children: [
                      SizedBox(
                        height: 100,
                        child: Image.asset(
                            "assets/images/Screenshot 2023-02-01 183125.png"),
                      ),
                      Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                                top: 20, bottom: 10, right: 15),
                            child: const Text(
                              "Hamburger",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Row(children: [
                            Container(
                              margin: const EdgeInsets.only(left: 20),
                              alignment: Alignment.center,
                              width: 20,
                              decoration: const BoxDecoration(
                                  color: Color.fromRGBO(255, 107, 1, 1),
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(40),
                                      bottomLeft: Radius.circular(40))),
                              child: const Text(
                                "-",
                                style: TextStyle(fontSize: 20, color: Colors.white),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topCenter,
                              // padding: const EdgeInsets.only(top: 10),
                              width: 40,
                              height: 25,
                              child: const Text(
                                "2",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 20),
                              ),
                            ),
                            Container(
                              height: 23,
                              alignment: Alignment.center,
                              width: 20,
                              decoration: const BoxDecoration(
                                  color: Color.fromRGBO(255, 107, 1, 1),
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(40),
                                      bottomRight: Radius.circular(40))),
                              child: const Text(
                                "+",
                                style: TextStyle(fontSize: 20, color: Colors.white),
                              ),
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              margin: const EdgeInsets.only(left: 10),
                              child: const Text(
                                "18.00",
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Color.fromRGBO(255, 107, 1, 1)),
                              ),
                            )
                          ]),
                        ],
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 65, top: 70),
                        decoration: const BoxDecoration(
                          borderRadius:
                              BorderRadius.only(topLeft: Radius.circular(10)),
                          color: Color.fromRGBO(255, 107, 1, 1),
                        ),
                        child: const Icon(
                          Icons.add,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 10, top: 20),
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(30))),
                  height: 100,
                  width: 400,
                  child: Row(
                    children: [
                      SizedBox(
                        height: 100,
                        child: Image.asset(
                            "assets/images/Screenshot 2023-02-01 183125.png"),
                      ),
                      Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                                top: 20, bottom: 10, right: 15),
                            child: const Text(
                              "Hamburger",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Row(children: [
                            Container(
                              margin: const EdgeInsets.only(left: 20),
                              alignment: Alignment.center,
                              width: 20,
                              decoration: const BoxDecoration(
                                  color: Color.fromRGBO(255, 107, 1, 1),
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(40),
                                      bottomLeft: Radius.circular(40))),
                              child: const Text(
                                "-",
                                style: TextStyle(fontSize: 20, color: Colors.white),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topCenter,
                              // padding: const EdgeInsets.only(top: 10),
                              width: 40,
                              height: 25,
                              child: const Text(
                                "2",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 20),
                              ),
                            ),
                            Container(
                              height: 23,
                              alignment: Alignment.center,
                              width: 20,
                              decoration: const BoxDecoration(
                                  color: Color.fromRGBO(255, 107, 1, 1),
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(40),
                                      bottomRight: Radius.circular(40))),
                              child: const Text(
                                "+",
                                style: TextStyle(fontSize: 20, color: Colors.white),
                              ),
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              margin: const EdgeInsets.only(left: 10),
                              child: const Text(
                                "18.00",
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Color.fromRGBO(255, 107, 1, 1)),
                              ),
                            )
                          ]),
                        ],
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 65, top: 70),
                        decoration: const BoxDecoration(
                          borderRadius:
                              BorderRadius.only(topLeft: Radius.circular(10)),
                          color: Color.fromRGBO(255, 107, 1, 1),
                        ),
                        child: const Icon(
                          Icons.add,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 40, left: 20),
                  child: const Text(
                    "Selected item (6)",
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 40, left: 130),
                  child: const Text(
                    "78.00",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                )
              ],
            ),
            Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 15, left: 20),
                  child: const Text(
                    "Delivery & handling",
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 15, left: 115),
                  child: const Text(
                    "10.00",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                )
              ],
            ),
            Container(
                margin: EdgeInsets.only(top: 10),
                child: Text(
                  "-------------------------------",
                  style: TextStyle(fontSize: 40),
                )),
            Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 20),
                  child: const Text(
                    "Subtotal",
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 210),
                  child: const Text(
                    "88.00",
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(255, 107, 1, 1)),
                  ),
                )
              ],
            ),
            Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.only(top: 40, left: 0),
              height: 60,
              width: 350,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(40)),
                color: Color.fromRGBO(255, 107, 1, 1),
              ),
              child: const Text(
                "Get Started",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
// ,
