import 'package:flutter/material.dart';
import 'package:food/second_page_layout.dart';

class FirstPageLayout extends StatelessWidget {
  const FirstPageLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                color: Colors.white,
              ),
              Container(
                  height: 300,
                  margin: const EdgeInsets.only(left: 70, top: 50),
                  child:
                      Image.asset("assets/images/Screenshot 2023-02-01 173804.png")),
              Container(
                  margin: const EdgeInsets.only(top: 450),
                  child: const Text(
                    "Quick and fastest delivery food for you",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  margin: const EdgeInsets.only(top: 550),
                  child: const Text(
                    "Get your favourite food fastest way possible.",
                    style: TextStyle(fontSize: 20),
                    textAlign: TextAlign.center,
                  )),
              InkWell(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                    return SecondPageLayout();
                  },));
                },
                child: Container(
                  alignment: Alignment.center,
                  margin: const EdgeInsets.only(top: 650, left: 20),
                  height: 60,
                  width: 350,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(40)),
                    color: Color.fromRGBO(255, 107, 1, 1),
                  ),
                  child: const Text(
                    "Get Started",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
