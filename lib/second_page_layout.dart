import 'package:flutter/material.dart';
import 'package:food/api/api_demo_http.dart';
import 'package:food/third_page_layout.dart';

class SecondPageLayout extends StatelessWidget {
  const SecondPageLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            color: const Color.fromRGBO(253, 253, 253, 1),
            child: Column(children: [
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 30, left: 20),
                      child: const Icon(Icons.menu, size: 40),
                    ),
                    Container(
                      height: 50,
                      width: 50,
                      margin: const EdgeInsets.only(top: 30, left: 270),
                      decoration: const BoxDecoration(
                          color: Color.fromRGBO(255, 107, 1, 1),
                          borderRadius: BorderRadius.all(Radius.circular(50))),
                      child: const Icon(
                        Icons.search,
                        size: 40,
                        color: Colors.white,
                      ),
                    )
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 30, left: 20),
                alignment: Alignment.centerLeft,
                child: const Text(
                  "Find your food",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Row(
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => ApiDemoHttp(),));
                        },
                        child: Container(
                          height: 50,
                          width: 50,
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(40))),
                          child: const Icon(
                            Icons.food_bank_outlined,
                            size: 40,
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) {
                              return const ThirdPageLayout();
                            },
                          ));
                        },
                        child: Container(
                          margin: const EdgeInsets.only(left: 25),
                          height: 50,
                          width: 50,
                          decoration: const BoxDecoration(
                              color: Color.fromRGBO(255, 107, 1, 1),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(40))),
                          child: const Icon(Icons.emoji_food_beverage),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 25),
                        height: 50,
                        width: 50,
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(40))),
                        child: const Icon(Icons.fastfood),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 25),
                        height: 50,
                        width: 50,
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(40))),
                        child: const Icon(Icons.no_food_outlined),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 25),
                        height: 50,
                        width: 50,
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(40))),
                        child: const Icon(Icons.add_business_sharp),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 20, top: 20),
                alignment: Alignment.centerLeft,
                child: const Text(
                  "Popular Food",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 10, top: 20),
                      decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(30))),
                      height: 220,
                      width: 180,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 100,
                            child: Image.asset(
                                "assets/images/Screenshot 2023-02-01 183107.png"),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 20),
                            child: const Text(
                              "Mushrooms",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 10),
                            child: const Text(
                              "12.00",
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromRGBO(255, 107, 1, 1)),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 150, top: 15),
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10)),
                              color: Color.fromRGBO(255, 107, 1, 1),
                            ),
                            child: const Icon(
                              Icons.add,
                              color: Colors.white,
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 10, top: 20),
                      decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(30))),
                      height: 220,
                      width: 180,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 100,
                            child: Image.asset(
                                "assets/images/Screenshot 2023-02-01 183125.png"),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 25),
                            child: const Text(
                              "Hawaiian",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 10),
                            child: const Text(
                              "21.00",
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromRGBO(255, 107, 1, 1)),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 150, top: 10),
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10)),
                              color: Color.fromRGBO(255, 107, 1, 1),
                            ),
                            child: const Icon(
                              Icons.add,
                              color: Colors.white,
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 20, left: 20),
                alignment: Alignment.centerLeft,
                child: const Text(
                  "Top of the week",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
              ),
              Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 10, top: 20),
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(30))),
                    height: 100,
                    width: 400,
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          SizedBox(
                            height: 100,
                            child: Image.asset(
                                "assets/images/Screenshot 2023-02-01 183125.png"),
                          ),
                          Column(
                            children: [
                              Container(
                                margin:
                                    const EdgeInsets.only(top: 20, left: 20),
                                child: const Text(
                                  "Hamburger",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                margin:
                                    const EdgeInsets.only(right: 25, top: 10),
                                child: const Text(
                                  "18.00",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Color.fromRGBO(255, 107, 1, 1)),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 110, top: 70),
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10)),
                              color: Color.fromRGBO(255, 107, 1, 1),
                            ),
                            child: const Icon(
                              Icons.add,
                              color: Colors.white,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 10, top: 20),
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(30))),
                    height: 100,
                    width: 400,
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          SizedBox(
                            height: 100,
                            child: Image.asset(
                                "assets/images/Screenshot 2023-02-01 183125.png"),
                          ),
                          Column(
                            children: [
                              Container(
                                margin:
                                    const EdgeInsets.only(top: 20, left: 20),
                                child: const Text(
                                  "Hawaiian Pizza",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                margin:
                                    const EdgeInsets.only(right: 15, top: 10),
                                child: const Text(
                                  "21.00",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Color.fromRGBO(255, 107, 1, 1)),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 75, top: 70),
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10)),
                              color: Color.fromRGBO(255, 107, 1, 1),
                            ),
                            child: const Icon(
                              Icons.add,
                              color: Colors.white,
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              )
            ]),
          ),
        ),
      ),
    );
  }
}
